#!/bin/bash
set -e

# Pipe version - This field is changed by the CI which expects this exact syntax. AVOID changing manually.
PIPE_VERSION="3.0.0"

DEFAULT_CLI_VERSION="2.8.0"

# Get custom CLI version, if set
JFROG_CLI_VERSION=${JFROG_CLI_VERSION:=$DEFAULT_CLI_VERSION}

# Download JFrog CLI and move the executable to PATH
curl -sSfLO https://releases.jfrog.io/artifactory/jfrog-cli/v2/$JFROG_CLI_VERSION/jfrog-cli-linux-amd64/jfrog | bash -s $JFROG_CLI_VERSION
chmod 777 jfrog
# Verify CLI was downloaded
if [ ! -f ./jfrog ]; then
    echo "JFrog CLI downloaded failed."
    exit 1
fi
mv ./jfrog /usr/bin/

# Loop and import all config tokens
while IFS='=' read -r name value ; do
  if [[ $name == 'JF_ARTIFACTORY_'* ]]; then
    jfrog config import ${!name}
  fi
done < <(env)

# Export env var
export JFROG_CLI_BUILD_NAME=${JFROG_CLI_BUILD_NAME:=$BITBUCKET_REPO_OWNER-$BITBUCKET_REPO_SLUG-$BITBUCKET_BRANCH}
export JFROG_CLI_BUILD_NUMBER=${JFROG_CLI_BUILD_NUMBER:=$BITBUCKET_BUILD_NUMBER}
export JFROG_CLI_BUILD_URL=${JFROG_CLI_BUILD_URL:="https://bitbucket.org/${BITBUCKET_REPO_OWNER}/${BITBUCKET_REPO_SLUG}/addon/pipelines/home#!/results/${BITBUCKET_BUILD_NUMBER}"}
export JFROG_CLI_USER_AGENT="bitbucket-pipelines/${PIPE_VERSION}"

# Delete this script after it's done executing
rm ./jfrog-setup-cli.sh