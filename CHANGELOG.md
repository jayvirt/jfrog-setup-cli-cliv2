# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 3.0.0

- major: BIG STUFF

## 2.0.0

- major: BIG STUFF

## 1.1.0

- major: Added JFrog CLI V2 Support

## 1.0.0

- major: Initial Release
