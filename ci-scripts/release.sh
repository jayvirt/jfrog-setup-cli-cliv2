#!/usr/bin/env bash

set -ex
IMAGE=$1

##
# Step 1: Generate new version
##
previous_version=$(semversioner current-version)
semversioner add-change -t major -d "BIG STUFF"
semversioner release
new_version=$(semversioner current-version)

##
# Step 2: Generate CHANGELOG.md
##
echo "Generating CHANGELOG.md file..."
semversioner changelog > CHANGELOG.md
# Use new version in the README.md examples
echo "Replace version '$previous_version' to '$new_version' in README.md ..."
sed -i "s/bbcliv2:[0-9]*\.[0-9]*\.[0-9]*/bbcliv2:$new_version/g" README.md
# Use new version in the pipe.yml metadata file
echo "Replace version '$previous_version' to '$new_version' in pipe.yml ..."
sed -i "s/bbcliv2:[0-9]*\.[0-9]*\.[0-9]*/bbcliv2:$new_version/g" pipe.yml
# Use new version in the jfrog-setup-cli.sh
echo "Replace version '$previous_version' to '$new_version' in jfrog-setup-cli.sh ..."
sed -i "s/PIPE_VERSION=\"[0-9]*\.[0-9]*\.[0-9]*\"/PIPE_VERSION=\"$new_version\"/g" pipe/jfrog-setup-cli.sh

##
# Step 3: Build and push docker image
##
echo "Build and push docker image..."
echo ${DOCKERHUB_PASSWORD} | docker login --username "$DOCKERHUB_USERNAME" --password-stdin
docker build -t ${IMAGE} .
docker tag ${IMAGE} ${IMAGE}:${new_version}
docker push ${IMAGE}

##
# Step 4: Commit back to the repository
##
echo "Committing updated files to the repository..."
git add .
git commit -m "Update files for new version '${new_version}' [skip ci]"
git push origin ${BITBUCKET_BRANCH}

##
# Step 5: Tag the repository
##
echo "Tagging for release ${new_version}" "${new_version}"
git tag -a -m "Tagging for release ${new_version}" "${new_version}"
git push origin ${new_version}
